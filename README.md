# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
1.筆刷、顏色選擇、粗細、橡皮擦功能都在右下角(橡皮擦功能可以選擇直線、三角形、方形、圓形、筆刷方式逕行擦拭)
(筆刷粗細和顏色選擇有用到jquery 網址 : http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.6.4.js)
2.打字功能點選text後再點選畫布可以打字，可以改變顏色和粗細
3.游標圖案會隨著不同功能有所不同
4.reset按鈕可以清空畫布
5.圓形、三角形、方形功能可以選擇顏色、粗細逕行拖拽即可構圖
6.undo、redo功能點選按鈕兩下就可以執行上一步和下一步
7.
8.點選download按鈕即可以下載成png檔
*其他功能:
直線功能 : 點選line後可以畫出筆直的線
彩虹功能 : 還沒有做完，只有辦法隨機變換畫筆粗細，無法變換顏色
複製功能 : 點選copy your pic可以複製目前畫布中的圖片到左下角，在左下角案滑鼠右鍵可以下載成png檔，而且reset時不會清空到左下角複製的圖片
網狀功能 : 點選fishnet功能，用鼠標畫一圈可以畫出漁網形狀的圓柱體


